const { Ticket } = require('../models/sequelize.js')
const Messages = require('../main/messages.js')

var buyTicket = function (id, count) {
	return new Promise((resolve, reject) => {
		getTickets(id).then(tickets => {
			ticket_counts = tickets.length
			new_tickets = []
			for (let i = 0; i < count; i++) {
				ticket_id = id.toString() + (ticket_counts + i + 1).toString()
				Ticket.create({
					ticket_id : ticket_id,
					user_id : id,
					price: Messages.TICKET_PRICE
				}).then( ticket => {
					new_tickets.push(ticket)
					if (new_tickets.length == count) {
						resolve(new_tickets)
					}
				})
			}
		})
	})
}

var getTickets = function (id) {
	return new Promise((resolve, reject) => {
		Ticket.findAll({
			where: {
				user_id : id
			}
		}).then( tickets => {
			resolve(tickets)
		})
	})

}


module.exports = {
	buyTicket,
	getTickets,
}
