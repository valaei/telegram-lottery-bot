const { User } = require('../models/sequelize.js')

var findUserById = function (id) {
	return new Promise((resolve, reject) => {
		User.findAll({
			where: {
				'user_id': id
			}}).then(user => {
				resolve(user)
			})
	})
}

var register =  function (id, name, family) {
	return new Promise((resolve, reject) => {
		findUserById(id).then(user => {
				if (user.length == 0) {
					User.create({
						user_id : id,
						name : name,
						family_name : family
					}).then(new_user => {
						resolve(new_user)
					})
				}				
				else {
					reject(Error("User " + id + " already exists!"))
				}
			})
	})
}
	




module.exports = {
	findUserById,
	register
}
