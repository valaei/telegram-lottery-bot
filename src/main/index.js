const Telegraf = require('telegraf')
const TOKEN = require('../token.js')
const MESSAGES = require('./messages.js')
const utils = require('./utils.js')
const UserController = require('../controller/UserController.js')
const TicketController = require('../controller/TicketController.js')


const bot = new Telegraf(TOKEN)

bot.use((ctx, next) => {
  const start = new Date()
  return next(ctx).then(() => {
    const ms = new Date() - start
    console.log('Response time %sms', ms)
  })
})

bot.start((ctx) => {
	let user = ctx.message.from
	let welcome = utils.parse(MESSAGES.START, user.first_name)
	ctx.reply(welcome, {
		parse_mode: 'HTML',
	})
	ctx.reply(MESSAGES.OPTIONS_TITLE_1, {
		parse_mode: 'HTML',
		reply_markup: {
			keyboard: MESSAGES.OPTIONS_LIST_1,
		}
	})
	UserController.register(user.id, user.first_name, user.last_name)
	
})

bot.on('text', (ctx) => {
	let user = ctx.message.from
	if (ctx.message.text == MESSAGES.BUY_TICKET) {
		return ctx.reply(MESSAGES.TICKET_COUNT, MESSAGES.REPLY_OPTIONS)
	}
	else if (ctx.message.text == MESSAGES.CHECK_STATUS) {
		bought_tickets = MESSAGES.BOUGHT_TICKETS
		TicketController.getTickets(user.id).then( tickets => {
			if (tickets.length == 0) {
				return ctx.reply(MESSAGES.NO_TICKET)
			} else {
				for(let i = 0; i < tickets.length ; i++) {
					ticket = tickets[i]
					bought_tickets += ticket.dataValues.ticket_id + "\n"
					if ( i == (tickets.length - 1)) {
						return ctx.reply(bought_tickets)
					}
				}
			}
			
		})
	}
	else if (ctx.message.text == MESSAGES.ABOUT) {
		return ctx.reply("Button 3 Works!")
	}
	else {
		ctx.reply(MESSAGES.ERROR, {
			parse_mode: 'HTML',
			reply_markup: {
				keyboard: MESSAGES.OPTIONS_LIST_1,
			}

		})
	}



})

bot.on("callback_query", (ctx) => {
	var answer = ctx.callbackQuery.data
	if (answer == 'YES') {
		var reg = /( 10 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 1 )/
		var text = ctx.callbackQuery.message.text
		var count = text.match(reg)[0]
		bought_tickets = MESSAGES.BOUGHT_TICKETS
		TicketController.buyTicket(ctx.callbackQuery.from.id, count).then( tickets => {
			for(let i = 0; i < tickets.length ; i++) {
				ticket = tickets[i]
				bought_tickets += ticket.dataValues.ticket_id + "\n"
				if ( i == (tickets.length - 1)) {
					return ctx.reply(bought_tickets)
				}
			}
			
		})
		
	}
	else if (answer == 'NO') {
		ctx.reply(MESSAGES.OPTIONS_TITLE_1, {
			parse_mode: 'HTML',
			reply_markup: {
				keyboard: MESSAGES.OPTIONS_LIST_1,
			}
		})
	}
	else {
		var count = Number(answer)
		var price = count * MESSAGES.TICKET_PRICE
		let question = utils.parse(MESSAGES.TICKET_PRICE_STATUS, price, count)
		ctx.reply(question, MESSAGES.REPLY_OPTIONS_2)
	}
})

bot.catch((err) => {
  console.log('ERROR: ', err)
})

bot.startPolling()

