const REPLY_OPTIONS = {
    reply_markup: {
        inline_keyboard: [
            [ { text: "۱",  callback_data: "1",  },
              { text: "۲",  callback_data: "2",  },
              { text: "۳",  callback_data: "3",  },
              { text: "۴",  callback_data: "4",  },
              { text: "۵",  callback_data: "5",  } ],
            [ { text: "۶",  callback_data: "6",  },
              { text: "۷",  callback_data: "7",  },
              { text: "۸",  callback_data: "8",  },
              { text: "۹",  callback_data: "9",  },
              { text: "۱۰", callback_data: "10", } ]
        ],
    },
};


const REPLY_OPTIONS_2 = {
    reply_markup: {
        inline_keyboard: [
            [ { text: "بله",  callback_data: "YES", } ],
            [ { text: "خیر", callback_data: "NO", } ]
        ],
    },
};

const BUY_TICKET = 'خرید برگ بخت آزمایی';
const CHECK_STATUS = 'مشاهده برگه های من';
const ABOUT = 'شرایط قرعه کشی';
const OPTIONS_TITLE_1 = '<b> لطفا یکی از موارد زیر را انتخاب کنید</b>';
const START = '<b>*.*.* سلام %s! به بات بخت آزمایی خوش آمدید *.*.*</b>';
const OPTIONS_LIST_1 = [[BUY_TICKET], [CHECK_STATUS], [ABOUT]];
const ERROR = '<b>درخواست شما معتبر نیست. لطفا یکی از موارد زیر را انتخاب کنید</b>';
const TICKET_COUNT = ' مایل به خرید چه تعداد برگ بخت آزمایی هستید؟ ';
const TICKET_PRICE = 1000;
const TICKET_PRICE_STATUS = 'آیا مایل به پرداخت %s ریال به ازای %s برگ بخت آزمایی هستید؟';
const BOUGHT_TICKETS = 'لیست برگه های خریداری شده \n'
const NO_TICKET = 'شما برگه ای خریداری نکرده اید'
module.exports = {
	REPLY_OPTIONS : REPLY_OPTIONS,
	REPLY_OPTIONS_2 : REPLY_OPTIONS_2,
	BUY_TICKET : BUY_TICKET,
	CHECK_STATUS : CHECK_STATUS,
	ABOUT : ABOUT,
	OPTIONS_TITLE_1 : OPTIONS_TITLE_1,
	START : START,
	OPTIONS_LIST_1 : OPTIONS_LIST_1,
	ERROR : ERROR,
	TICKET_COUNT : TICKET_COUNT,
	TICKET_PRICE : TICKET_PRICE,
	TICKET_PRICE_STATUS : TICKET_PRICE_STATUS,
	BOUGHT_TICKETS : BOUGHT_TICKETS,
	NO_TICKET : NO_TICKET
}
