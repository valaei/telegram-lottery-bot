module.exports = (sequelize, type) => {
	return sequelize.define('ticket', {
		id: {
			type: type.BIGINT(20),
			primaryKey: true,
			autoIncrement: true
		},
		ticket_id: {
			type: type.BIGINT(20),
			allowNull: false
		},
		user_id: {
			type: type.BIGINT(20),
			allowNull: false
		},
		price: {
			type: type.INTEGER,
			allowNull: false,

		}
	})

}
