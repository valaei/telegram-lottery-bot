module.exports = (sequelize, type) => {
	return sequelize.define('user', {
		id: {
			type: type.BIGINT(20),
			primaryKey: true,
			autoIncrement: true
		},
		user_id: {
			type: type.BIGINT(20),
			allowNull: false
		},
		name: {
			type: type.STRING,
			allowNull: true
		},
		family_name: {
			type: type.STRING,
			allowNull: true
		}

	})

}
