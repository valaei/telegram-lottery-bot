const Sequelize = require('sequelize')
const UserModel = require('./User.js')
const TicketModel = require('./Ticket.js')


const sequelize = new Sequelize('mysql://root:a12121374@localhost:1234/lottery')

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  });

const User = UserModel(sequelize, Sequelize)
const Ticket = TicketModel(sequelize, Sequelize)

sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })

module.exports = {
	User,
	Ticket
}
